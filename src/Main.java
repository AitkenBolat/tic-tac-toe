import java.util.Scanner;

public class Main {
    public static void printdesk(String[][] desk) {
        for (int i = 0; i < desk.length; i++) {
            for (int j = 0; j < desk[i].length; j++) {
                System.out.print(desk[i][j] + "|");
            }
            System.out.println();
        }
    }

    public static boolean checkifoccupied1(String[][] desk, int row1, int column1) {
        if (desk[row1][column1] == null) {
            return true;
        }
        return false;

    }

    public static boolean checkifoccupied2(String[][] desk, int row2, int column2) {
        if (desk[row2][column2] != null) {
            return false;
        }
        return true;
    }

    public static void play1(Scanner scanner, String[][] desk) {
        for (int i = 0; i <= 4; i++) {
            System.out.println("Choose row, player 1");
            int row1 = scanner.nextInt();
            System.out.println("Choose column, player 1");
            int column1 = scanner.nextInt();
            if (row1 > 2 || column1 > 2) {
                System.out.println("Incorrect row or column");
                System.out.println("Choose row or column between 0 and 2");
                i--;
            } else if (row1 < 0 || column1 < 0) {
                System.out.println("Incorrect row or column");
                System.out.println("Choose row or column between 0 and 2");
                i--;
            } else {
                boolean check1 = checkifoccupied1(desk, row1, column1);
                if (check1 == true) {
                    desk[row1][column1] = "X";
                    printdesk(desk);
                    break;
                } else {
                    System.out.println("The position is occupied, choose another");
                    i--;
                }
            }
        }
    }


    public static void play2(Scanner scanner, String[][] desk) {
        for (int i = 0; i <= 4; i++) {
            System.out.println("Choose row, player 2");
            int row2 = scanner.nextInt();
            System.out.println("Choose column, player 2");
            int column2 = scanner.nextInt();
            if (row2 > 2 || column2 > 2) {
                System.out.println("Incorrect row or column");
                System.out.println("Choose row or column between 0 and 2");
                i--;
            } else if (row2 < 0 || column2 < 0) {
                System.out.println("Incorrect row or column, restart the game");
                System.out.println("Choose row or column between 0 and 2");
                i--;
            } else {
                boolean check2 = checkifoccupied2(desk, row2, column2);
                if (check2 == true) {
                    desk[row2][column2] = "O";
                    printdesk(desk);
                    break;
                } else {
                    System.out.println("The position is occupied, choose another");
                    i--;
                }
            }
        }
    }

    public static boolean check1winrow(String[][] desk) {
        int count1 = 0;
        for (int i = 0; i < desk.length; i++) {
            for (int j = 0; j < desk[i].length; j++) {
                if (desk[i][j] == "X") {
                    count1++;
                } else {
                    count1 = 0;
                }
            }
            if (count1 == 3) {
                return true;
            }
        }
        return false;
    }

    public static boolean check1wincolumn(String[][] desk) {
        int count1 = 0;
        for (int i = 0; i < desk.length; i++) {
            for (int j = 0; j < desk[i].length; j++) {
                if (desk[j][i] == "X") {
                    count1++;
                } else {
                    count1 = 0;
                }
            }
            if (count1 == 3) {
                return true;
            }
        }
        return false;
    }

    public static boolean check1windiag(String[][] desk) {
        if (desk[0][0] == "X" && desk[1][1] == "X" && desk[2][2] == "X"
                || desk[0][2] == "X" && desk[1][1] == "X" && desk[2][0] == "X") {
            return true;
        }
        return false;
    }

    public static boolean check1win(String[][] desk) {
        boolean a = check1winrow(desk);
        boolean b = check1wincolumn(desk);
        boolean c = check1windiag(desk);
        if (a || b || c == true) {
            return true;
        }
        return false;
    }

    public static boolean check2winrow(String[][] desk) {
        int count2 = 0;
        for (int i = 0; i < desk.length; i++) {
            for (int j = 0; j < desk[i].length; j++) {
                if (desk[i][j] == "O") {
                    count2++;
                } else {
                    count2 = 0;
                }
            }
            if (count2 == 3) {
                return true;
            }
        }
        return false;
    }

    public static boolean check2wincolumn(String[][] desk) {
        int count2 = 0;
        for (int i = 0; i < desk.length; i++) {
            for (int j = 0; j < desk[i].length; j++) {
                if (desk[j][i] == "O") {
                    count2++;
                } else {
                    count2 = 0;
                }
            }
            if (count2 == 3) {
                return true;
            }
        }
        return false;
    }

    public static boolean check2windiag(String[][] desk) {
        if (desk[0][0] == "O" && desk[1][1] == "O" && desk[2][2] == "O"
                || desk[0][2] == "O" && desk[1][1] == "O" && desk[2][0] == "O") {
            return true;
        }
        return false;
    }

    public static boolean check2win(String[][] desk) {
        boolean a = check2wincolumn(desk);
        boolean b = check2winrow(desk);
        boolean c = check2windiag(desk);
        if (a || b || c == true) {
            return true;
        }
        return false;
    }

    public static boolean draw(String[][] desk) {
        int count = 0;
        for (int i = 0; i < desk.length; i++) {
            for (int j = 0; j < desk[i].length; j++) {
                if (desk[i][j] != null) {
                    count++;
                }
            }
        }
        if (count == 8) {
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("What's your name player 1");
        String player1 = scanner.next();
        System.out.println("What's your name player 2");
        String player2 = scanner.next();
        int size = 3;
        String[][] desk = new String[size][size];
        printdesk(desk);
        while (true) {
            if (draw(desk) == true) {
                System.out.println("It's a draw");
                break;
            } else {
                play1(scanner, desk);
                if (check1win(desk) == true) {
                    System.out.println(player1 + " have won");
                    break;
                }
                play2(scanner, desk);

                if (check2win(desk) == true) {
                    System.out.println(player2 + " have won");
                    break;
                }

            }
        }
    }
}



